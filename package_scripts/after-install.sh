#!/bin/bash

gsettings_set() {
  local scheme=$1
  local key=$2
  local value=$3
  export DISPLAY=":0"
  sudo -u lightdm -H /bin/sh -c 'eval `dbus-launch --auto-syntax` && gsettings set '"$scheme $key $value"'' > /dev/null
}

gsettings_set "com.canonical.unity-greeter background /usr/share/backgrounds/my-logo/background.png"
gsettings_set "com.canonical.unity-greeter draw-user-backgrounds false"
gsettings_set "com.canonical.unity-greeter draw-grid false"
gsettings_set "com.canonical.unity-greeter play-ready-sound false"
gsettings_set "com.canonical.unity-greeter font-name 'Noto Sans Regular 11'"
gsettings_set "com.canonical.unity-greeter theme-name Numix"
gsettings_set "com.canonical.unity-greeter icon-theme-name Numix-Circle"
gsettings_set "com.canonical.unity-greeter background-color '#333333'"
