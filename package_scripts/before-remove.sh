#!/bin/bash

gsettings_set() {
  local scheme=$1
  local key=$2
  local value=$3
  export DISPLAY=":0"
  sudo -u lightdm -H /bin/sh -c 'eval `dbus-launch --auto-syntax` && gsettings set '"$scheme $key $value"'' > /dev/null
}

gsettings_set "com.canonical.unity-greeter background /usr/share/backgrounds/warty-final-ubuntu.png"
gsettings_set "com.canonical.unity-greeter draw-user-backgrounds true"
gsettings_set "com.canonical.unity-greeter draw-grid true"
gsettings_set "com.canonical.unity-greeter play-ready-sound true"
gsettings_set "com.canonical.unity-greeter font-name 'Ubuntu 11'"
gsettings_set "com.canonical.unity-greeter theme-name Ambiance"
gsettings_set "com.canonical.unity-greeter icon-theme-name ubuntu-mono-dark"
gsettings_set "com.canonical.unity-greeter background-color '#2C001E'"
