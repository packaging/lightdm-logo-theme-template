# LightDM Unity Greeter logo template for Ubuntu

It might happen that you'll need to deploy some sort of corporate identity to Ubuntu clients (e.g. in schools, at work, at home, just for fun,...). Therefore it might also be easy, to deploy a package, which takes care of things rather than doing all manually. This package can then be triggerd by some orchestration tools (e.g. Puppet, Salt, Ansible, Chef, whatever,...) or directly when installing via Preseed. So far, so good, here's a simple template which basically just inserts a different background and set some theme settings, all using the gsettings/dconf stuff.

Together with [Plymouth Logo template](https://gitlab.com/packaging/plymouth-logo-theme-template/), this will provide a consistent, branded look from boot to login.

![](lightdm.png)

## Customization

Best thing is to create a new theme directory in ```package_root``` and adjust things there.

```
MY_CI_TARGET="acme-inc-logo"
cp -R package_root/my-logo package_root/$MY_CI_TARGET
```

Now you can replace ```package_root/$MY_CI_TARGET/background.png``` with yours.

You'll also need to have a look at some variables inside ```package_root/$MY_CI_TARGET/after-install.sh``` as this example is using a completely different theme:

* ```font-name```: self-explanatory, see dependencies[^1]
* ```background-color```: for better transition from LightDM startup until image is displayed, you might pick the most dominant color and add it here
* ```theme-name```: name of the GTK them, see dependencies[^1]
* ```icon-theme-name```: name of the icon theme to use, see dependencies[^1]

Of course, you can also delete the lines with settings, you do not want to change from the default (e.g. only set background).

You should also check the ```--depends $DEPENDENCY \``` section in ```create.sh``` to get rid of unnecessary example dependencies like ```numix-gtk-theme```.

[^1]: include as ```--depends $DEPENDENCY \``` in ```create.sh```

## Packaging

Just call the script ```create.sh``` with your theme directory as parameter. You should also set the variables ```VENDOR``` and ```MAINTAINER```. You'll need [ruby](http://rvm.io/rvm/install) and the fpm gem (```gem install fpm --no-ri --no-rdoc```). If you don't want to mess up your system, just use Docker:

```
docker run --rm -it -v /tmp/lightdm-logo-theme-template/:/build -w /build ruby:latest /bin/bash
apt-get update && apt-get install ruby ruby-dev -y
gem install fpm --no-ri --no-rdoc
VENDOR="ACME Inc." MAINTAINER="$VENDOR" ./create.sh "package_root/$MY_CI_TARGET"
```
